package pe.usil.android.googlemapsexample;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

public class Mapa extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = Mapa.class.getSimpleName();

    SupportMapFragment miMapa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);
        miMapa = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.miMapa);
        miMapa.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_mimapa));
            if (!success) {
                Log.e("Error:", "No se pudo enlazar el estilo al mapa");
            }
        } catch (Resources.NotFoundException ex) {
            Log.d("Error:", "No hay un estilo para el mapa.", ex);
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(-11.7562106, -77.1631062)));
    }
}
